(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('select').material_select();
    $('.modal-trigger').leanModal();
    $('ul.tabs').tabs();
    $('.modal').modal();

  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 16,
    format: 'dd/mm/yyyy' // Creates a dropdown of 15 years to control year
  });

  }); // end of document ready
})(jQuery); // end of jQuery name space

